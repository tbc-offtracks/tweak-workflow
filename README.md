This project tests advanced `[ci skip]` Git comments:

* `[ci skip on tag]` skips CI/CD on tag pipelines
* `[ci skip on prod]` skips CI/CD on the production branch
* `[ci skip on integ]` skips CI/CD on the integration branch
* `[ci skip on mr]` skips CI/CD on MR pipelines

Change with `[ci skip on tag]` comment
No on the first line of the Git comment
